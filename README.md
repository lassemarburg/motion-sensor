# Motion Sensor

## Requirements

PuSubClient

## Setup

Replace WiFi credentials in *MotionSensorMQTT.ino*

```c++
const char* ssid =  "yourSSID"; //use your ssid
const char* password = "yourPassword"; // use your password
```

And if need be the MQTT Broker URL

```c++
const char* mqtt_server = "test.mosquitto.org"; // use your MQTT Broker URL
```

Upload the sketch to your board.

## MQTT Topics and Messages

### From Motion Sensor

Every 5 Seconds

`devices/motion/heartbeat`

Motion Detected

`devices/motion/detected yes`

No Motion Detected

`devices/motion/detected no`
