#include <ESP8266WiFi.h> // Import ESP8266 WiFi library
#include <PubSubClient.h>// Import PubSubClient library to initialize MQTT protocol
// Update these with values suitable for your network.
const char* ssid =  "yourSSID"; //use your ssid
const char* password = "yourPassword"; // use your password
const char* mqtt_server = "test.mosquitto.org"; // use your MQTT Broker URL
WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE  (50)
char msg[MSG_BUFFER_SIZE];
int value = 0;

/*
 * 1 Button Shield - Simple Push
 * Press the pushbutton to switch on the LED
 *
 * 1 Button Shield pushbutton connects pin D3 to GND
 */

const int PIR = D3;


int PIRState = 0;
int PIRStatus = 0;

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// Check for Message received on define topic for MQTT Broker
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(),"device","6jsFn590OUwv0ZFX0J")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("devices/motion/connected", "hello world");
      // ... and resubscribe
      client.subscribe("devices/motion");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200);

  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  pinMode(PIR, INPUT);
  pinMode(BUILTIN_LED, OUTPUT);
  
  // set initial state, LED off
  digitalWrite(BUILTIN_LED, HIGH);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  unsigned long now = millis();
  if (now - lastMsg > 5000) {
    lastMsg = now;
    ++value;
    snprintf (msg, MSG_BUFFER_SIZE, "#%ld", value);
    client.publish("devices/motion/heartbeat", msg);
  }

  PIRState = digitalRead(PIR);

  if (PIRState == HIGH && PIRStatus != PIRState) {
    Serial.println("ON");
    client.publish("devices/motion/detected", "yes");
    digitalWrite(BUILTIN_LED, LOW);  // LED on
    PIRStatus = PIRState;
  } else if (PIRStatus != PIRState){
    Serial.println("OFF");
    client.publish("devices/motion/detected", "no");
    digitalWrite(BUILTIN_LED, HIGH); // LED off
    PIRStatus = PIRState;
  }
}